#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <random>

#include <faiss/IndexLSH.h>
#include <faiss/IndexSimpleLsh.h>

using idx_t = faiss::Index::idx_t;

int main(){
    int d = 128;      // dimension
    int nb = 100000; // database size
    int nq = 23;  // nb of queries

    std::mt19937 rng;
    std::uniform_real_distribution<> distrib;

    // float** xb = new float*[ nb];
    // float** xq = new float*[ nq];
    // for(int i = 0; i < nb; i++){
    //     xb[i] = new float[d];
    // }
    // for(int i = 0; i < nq; i++){
    //     xq[i] = new float[d];
    // }

    // //构建数据库的底层向量
    // for (int i = 0; i < nb; i++) {
    //     for (int j = 0; j < d; j++)
    //         xb[i][j] = distrib(rng);
    //     // xb[d * i] += i / 1000.;
    // }

    // //构建查询向量
    // for (int i = 0; i < nq; i++) {
    //     for (int j = 0; j < d; j++)
    //         xq[i][j] = distrib(rng);
    // }

    float* xb = new float[d * nb];
    float* xq = new float[d * nq];

    for (int i = 0; i < nb; i++) {
        for (int j = 0; j < d; j++)
            xb[d * i + j] = distrib(rng);
        // xb[d * i] += i / 1000.;
    }

    for (int i = 0; i < nq; i++) {
        for (int j = 0; j < d; j++)
            xq[d * i + j] = distrib(rng);
        // xq[d * i] += i / 1000.;
    }

    int nbits = 128;    //每个向量的比特数
    int k = 4;

    // faiss::IndexLSH index(d,nbits);
    // index.train(nb,xb);
    // index.add(nb,xb);

    
    faiss::IndexSimpleLsh index(nb,d);
    index.train(512);
    index.add(nb,xb);

    {   //search xq
        float* D = new float[k * nq];
        int* I = new int[k * nq];
        
        index.search(nq,xq,k,D,I);
        
        printf("I=\n");
        for (int i =0; i < 5; i++) {
            for (int j = 0; j < k; j++)
                printf("%5zd ", I[i * k + j]);
            printf("\n");
        }

        printf("D=\n");
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < k; j++)
                printf("%7g ", D[i * k + j]);
            printf("\n");
        }

        delete[] I;
        delete[] D;
    }

    // for(int i = 0; i < nb; i++){
    //     delete[] xb[i]; xb = nullptr;
    // }
    // for(int i = 0; i < nq; i++){
    //     delete xq[i]; xq = nullptr;
    // }
    delete[] xb;
    delete[] xq;


}