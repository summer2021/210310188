#pragma once

#include <iostream>
#include <algorithm>
#include <cmath>
#include <cassert>
#include <vector>
#include <chrono>

#include <faiss/utils/def.h>
#include <faiss/utils/utils.h>
#include <faiss/utils/pri_queue.h>
#include <faiss/IndexSRPLsh.h>

namespace faiss{
    
// -----------------------------------------------------------------------------
//  Simple-LSH is used to solve the problem of c-Approximate Maximum Inner 
//  Product (c-AMIP) search.
//
//  the idea was introduced by Behnam Neyshabur and Nathan Srebro in their 
//  paper "On Symmetric and Asymmetric LSHs for Inner Product Search", In 
//  Proceedings of the 32nd International Conference on International Conference 
//  on Machine Learning (ICML), pages 1926–1934, 2015.
// -----------------------------------------------------------------------------
class IndexSimpleLsh{
public:
    int n_pts_;
    int dim_l;
    float M_;
    int K_;                  //number of hash functions
    IndexSRPLsh *lsh_;
    float **norm_d;
    float **data_ ;

    IndexSimpleLsh(         // constructor
        int n,              // number of data objects
        int d);              // dimensionality
    
    ~IndexSimpleLsh();

    void train(int k);

    void add(int n,const float** x);
    void add(int n,const float* x);

    void display();

    void search(
        int n,
        const float** q,
        int top_k,
        float* D,
        int* I);

    void search(
        int n,
        const float* q,
        int top_k,
        float* D,
        int* I);
};


}