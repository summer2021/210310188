#include "IndexSimpleLsh.h"

namespace faiss{

// -----------------------------------------------------------------------------
IndexSimpleLsh::IndexSimpleLsh(
    int n,
    int d)
    :n_pts_(n),
    dim_l(d),
    norm_d(nullptr),
    data_(nullptr),
    lsh_(nullptr){
    
}


IndexSimpleLsh::~IndexSimpleLsh()
{
    if(norm_d != NULL){
        for(int i = 0; i<n_pts_;i++){
            delete[] norm_d[i];norm_d[i] = NULL;
        }
        delete[] norm_d; norm_d = NULL;
    }

    if(data_!=NULL){
        for(int i = 0; i<n_pts_;i++){
            delete[] data_[i];data_[i] = NULL;
        }
        delete[] data_; data_ = NULL;
    }
    
    if (lsh_ != NULL) { delete lsh_; lsh_ = NULL; }
}

// -----------------------------------------------------------------------------
void IndexSimpleLsh::train(
    int k)
{
    // -------------------------------------------------------------------------
	//  init srp_lsh
	// -------------------------------------------------------------------------
    lsh_ = new IndexSRPLsh(n_pts_,dim_l);
    this->K_ = k;
    lsh_->train(k);
    lsh_->display();
}

void IndexSimpleLsh::add(int n, const float** x){

    assert(n==n_pts_);
    assert(x!=nullptr);
    data_ = new float*[n];
    for(int i = 0; i<n;i++){
        data_[i] = new float[dim_l];
        memcpy(data_[i], x[i], dim_l*SIZEFLOAT);
    }

    // -------------------------------------------------------------------------
	//  calculate norm_l2 of data
	// -------------------------------------------------------------------------
    norm_d = new float*[n];
    int i = 0;
    for(i = 0; i < n; ++i){
        norm_d[i] = new float[NORM_K];
    }
    float tmp = 0.0f;
    for(i = 0; i < n; ++i){
        norm_d[i] = new float[NORM_K];
        memset(norm_d[i], 0.0f, NORM_K * SIZEFLOAT);
        for(int j = 0; j < dim_l; ++j){
            tmp = x[i][j];

            norm_d[i][0] += SQR(tmp);
            for (int t = 1; t < NORM_K; ++t) {
				if (j < 8 * t) norm_d[i][t] += SQR(tmp);
			}
        }

        for (int t = 1; t < NORM_K; ++t) {
            norm_d[i][t] = sqrt(norm_d[i][0] - norm_d[i][t]);
        }
        norm_d[i][0] = sqrt(norm_d[i][0]);
    }

    // -------------------------------------------------------------------------
	//  calculate the Euclidean norm of data and find the maximum norm of data
	// -------------------------------------------------------------------------
    float *norm = new float[n];
    float max_norm = MINREAL;
    for (i = 0; i < n_pts_; ++i) {
		norm[i] =norm_d[i][0];
		if (norm[i] > max_norm) max_norm = norm[i];
	}
    M_ = max_norm;
    // -------------------------------------------------------------------------    
	//  build hash tables for srp_lsh for new format of data
	// -------------------------------------------------------------------------
    bool  *hash_code = new bool[K_];
	float *simple_lsh_data = new float[dim_l + 1];

    for (i = 0; i < n_pts_; ++i) {
		// construct new format of data by simple-lsh transformation
		for (int j = 0; j < dim_l; ++j) {
			simple_lsh_data[j] = x[i][j] / M_;
		}
		simple_lsh_data[dim_l] = sqrt(1.0f - norm[i] / max_norm);

		// calc hash key for this new format of data
		for (int j = 0; j < K_; ++j) {
			hash_code[j] = lsh_->calc_hash_code(j, simple_lsh_data);
		}
		lsh_->compress_hash_code((const bool*) hash_code, lsh_->hash_key_[i]);
	}

    delete[] norm;
    delete[] hash_code;
	delete[] simple_lsh_data;
}


void IndexSimpleLsh::add(int n, const float* x){

    assert(n==n_pts_);
    assert(x!=nullptr);
    data_ = new float*[n];
    for(int i = 0; i<n;i++){
        data_[i] = new float[dim_l];
        memcpy(data_[i], x+i*dim_l, dim_l*SIZEFLOAT);
    }

    // -------------------------------------------------------------------------
	//  calculate norm_l2 of data
	// -------------------------------------------------------------------------
    norm_d = new float*[n];
    int i = 0;
    for(i = 0; i < n; ++i){
        norm_d[i] = new float[NORM_K];
    }
    float tmp = 0.0f;
    for(i = 0; i < n; ++i){
        norm_d[i] = new float[NORM_K];
        memset(norm_d[i], 0.0f, NORM_K * SIZEFLOAT);
        for(int j = 0; j < dim_l; ++j){
            tmp = x[i*dim_l+j];

            norm_d[i][0] += SQR(tmp);
            for (int t = 1; t < NORM_K; ++t) {
				if (j < 8 * t) norm_d[i][t] += SQR(tmp);
			}
        }

        for (int t = 1; t < NORM_K; ++t) {
            norm_d[i][t] = sqrt(norm_d[i][0] - norm_d[i][t]);
        }
        norm_d[i][0] = sqrt(norm_d[i][0]);
    }

    // -------------------------------------------------------------------------
	//  calculate the Euclidean norm of data and find the maximum norm of data
	// -------------------------------------------------------------------------
    float *norm = new float[n];
    float max_norm = MINREAL;
    for (i = 0; i < n_pts_; ++i) {
		norm[i] =norm_d[i][0];
		if (norm[i] > max_norm) max_norm = norm[i];
	}
    M_ = max_norm;
    // -------------------------------------------------------------------------    
	//  build hash tables for srp_lsh for new format of data
	// -------------------------------------------------------------------------
    bool  *hash_code = new bool[K_];
	float *simple_lsh_data = new float[dim_l + 1];

    for (i = 0; i < n_pts_; ++i) {
		// construct new format of data by simple-lsh transformation
		for (int j = 0; j < dim_l; ++j) {
			simple_lsh_data[j] = x[i*dim_l+j] / M_;
		}
		simple_lsh_data[dim_l] = sqrt(1.0f - norm[i] / max_norm);

		// calc hash key for this new format of data
		for (int j = 0; j < K_; ++j) {
			hash_code[j] = lsh_->calc_hash_code(j, simple_lsh_data);
		}
		lsh_->compress_hash_code((const bool*) hash_code, lsh_->hash_key_[i]);
	}

    delete[] norm;
    delete[] hash_code;
	delete[] simple_lsh_data;
}


// -----------------------------------------------------------------------------
void IndexSimpleLsh::display()		// display parameters
{
	printf("Parameters of Simple_LSH:\n");
	printf("    n = %d\n",   n_pts_);
	printf("    d = %d\n",   dim_l);
	printf("    M = %f\n\n", M_);
    printf("    K = %d\n\n", K_);
}

void IndexSimpleLsh::search(
    int qn,
    const  float** q,
    int top_k,
    float *D,
    int *I)
{
    // -------------------------------------------------------------------------
	//  calculate norm_l2 of quary
	// -------------------------------------------------------------------------
    float **norm_q;
    norm_q = new float*[qn];
    int i = 0;
    for(i = 0; i < qn; ++i){
        norm_q[i] = new float[NORM_K];
    }
    float tmp = 0.0f;
    for(i = 0; i < qn; ++i){
        // norm_q[i] = new float[NORM_K];
        memset(norm_q[i], 0.0f, NORM_K * SIZEFLOAT);
        for(int j = 0; j < dim_l; ++j){
            tmp = q[i][j];

            norm_q[i][0] += SQR(tmp);
            for (int t = 1; t < NORM_K; ++t) {
				if (j < 8 * t) norm_q[i][t] += SQR(tmp);
			}
        }

        for (int t = 1; t < NORM_K; ++t) {
            norm_q[i][t] = sqrt(norm_q[i][0] - norm_q[i][t]);
        }
        norm_q[i][0] = sqrt(norm_q[i][0]);
    }

    // -------------------------------------------------------------------------
	//  construct Simple_LSH query
	// -------------------------------------------------------------------------
    float *simple_lsh_query = new float[dim_l + 1];
    MaxK_List *list = new MaxK_List(top_k);
    for(int i = 0; i < qn; i++){

        float normq = norm_q[i][0];
        memset(simple_lsh_query,0,SIZEFLOAT*(dim_l+1));
        for (int j = 0; j < dim_l; ++j) {
            simple_lsh_query[j] = q[i][j] / normq;
        }
        simple_lsh_query[dim_l] = 0.0f;

        // -------------------------------------------------------------------------
        //  conduct c-k-AMC search by SRP-LSH
        // -------------------------------------------------------------------------
        std::vector<int> cand;
        lsh_->kmc(top_k, (const float *) simple_lsh_query, cand);

        // -------------------------------------------------------------------------
        //  calc inner product for candidates returned by SRP-LSH
        // -------------------------------------------------------------------------
        float kip  = MINREAL;
        int   size = (int) cand.size();
        list->reset();
        for (int j = 0; j < size; ++j) {
            int id = cand[j];
            if (norm_d[id][0] * normq <= kip) break;
                    
            float ip = calc_inner_product(dim_l, kip, data_[id], norm_d[id], 
                q[id], norm_q[id]);
            kip = list->insert(ip, id + 1);
        }
        for(int j = 0; j < top_k; j++){
            I[i*top_k+j] = list->ith_id(j);
            D[i*top_k+j] = list->ith_key(j);
        }
    }
     delete[] simple_lsh_query;
     delete list;

}

void IndexSimpleLsh::search(
    int qn,
    const  float* q,
    int top_k,
    float *D,
    int *I)
{
    // -------------------------------------------------------------------------
	//  calculate norm_l2 of quary
	// -------------------------------------------------------------------------
    float **norm_q;
    norm_q = new float*[qn];
    int i = 0;
    for(i = 0; i < qn; ++i){
        norm_q[i] = new float[NORM_K];
    }
    float tmp = 0.0f;
    for(i = 0; i < qn; ++i){
        // norm_q[i] = new float[NORM_K];
        memset(norm_q[i], 0.0f, NORM_K * SIZEFLOAT);
        for(int j = 0; j < dim_l; ++j){
            tmp = q[i*dim_l+j];

            norm_q[i][0] += SQR(tmp);
            for (int t = 1; t < NORM_K; ++t) {
				if (j < 8 * t) norm_q[i][t] += SQR(tmp);
			}
        }

        for (int t = 1; t < NORM_K; ++t) {
            norm_q[i][t] = sqrt(norm_q[i][0] - norm_q[i][t]);
        }
        norm_q[i][0] = sqrt(norm_q[i][0]);
    }
    
    // -------------------------------------------------------------------------
	//  construct Simple_LSH query
	// -------------------------------------------------------------------------
    float *simple_lsh_query = new float[dim_l + 1];
    MaxK_List *list = new MaxK_List(top_k);
    for(i = 0; i < qn; i++){

        float normq = norm_q[i][0];
        memset(simple_lsh_query,0,SIZEFLOAT*(dim_l+1));
        for (int j = 0; j < dim_l; ++j) {
            simple_lsh_query[j] = q[i*dim_l+j] / normq;
        }
        simple_lsh_query[dim_l] = 0.0f;

        // -------------------------------------------------------------------------
        //  conduct c-k-AMC search by SRP-LSH
        // -------------------------------------------------------------------------
        std::vector<int> cand;
        lsh_->kmc(top_k, (const float *) simple_lsh_query, cand);
        
        // -------------------------------------------------------------------------
        //  calc inner product for candidates returned by SRP-LSH
        // -------------------------------------------------------------------------
        float kip  = MINREAL;
        int   size = (int) cand.size();
        list->reset();
        for (int j = 0; j < size; ++j) {
            int id = cand[j];
            if (norm_d[id][0] * normq <= kip) break;
                    
            float ip = calc_inner_product(dim_l, kip, data_[id], norm_d[id], 
                q+i, norm_q[i]);
            kip = list->insert(ip, id + 1);
        }
        for(int j = 0; j < top_k; j++){
            I[i*top_k+j] = list->ith_id(j);
            D[i*top_k+j] = list->ith_key(j);
        }
    }
    delete[] simple_lsh_query;
    delete list;

    for(int i = 0; i < qn; i++){
        delete[] norm_q[i]; norm_q[i] = nullptr;
    }
    delete norm_q;

}

}